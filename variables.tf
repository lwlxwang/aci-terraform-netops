variable "tenant_name" {
  type = string
}
variable "aci_username" {
  type = string
}
variable "aci_password" {
  type = string
}
variable "apic_url" {
  type = string
}
variable "aws_region" {
  type = string
}
variable "aws_access_key" {
  type = string
}
variable "aws_secret_key" {
  type = string
}
variable "account_id" {
  type = string
}
variable "vrf" {
  type = string
}
variable "bd" {
  type = string
}
variable "cloud_zone" {
  type = string
}
variable "subnet" {
  type = string
}
variable "cidr" {
  type = string
}
