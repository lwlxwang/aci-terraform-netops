provider "aci" {
  username = var.aci_username
  password = var.aci_password
  url      = var.apic_url
  insecure = true
  version  = "0.2.1"
}
