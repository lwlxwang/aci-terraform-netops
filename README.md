#  Network team repo provisioning of ACI Network on Cloud APIC running  on AWS 

This is the terraform repo for Network team to provision basic network services on cloud APIC running on AWS.

This refers to the Day0 task of setting up the tenant, vrf, bd, cloud-context-profile, cidr and subnet.
Creation ot the above ACI constructs will result in creation of a VPC , CIDR and Subnet on AWS


### Outputs

Network teams makes some variables available to external teams to consume.

This output includes:
```
    aws_region 
    bd_id 
    bd_name 
    cloud_zone 
    subnet 
    tenant_id 
    tenant_name 
    vpc 
    vrf_id 
    vrf_name
```
example output:

```
    aws_region = us-west-1
    bd_id = uni/tn-CISCO-it/BD-bd1
    bd_name = bd1
    cloud_zone = us-west-1a
    subnet = subnet-[10.230.231.1/24]
    tenant_id = uni/tn-CISCO-it
    tenant_name = CISCO-it
    vpc = context-[admin-vrftf]-addr-[10.230.231.1/16]
    vrf_id = uni/tn-CISCO-it/ctx-admin-vrftf
    vrf_name = admin-vrftf
```

### To use the outputs, other modules should refer to the following remote backend:

```
    data "terraform_remote_state" "networking" {
      backend = "remote"
      config = {
        organization = "cisco-dcn-ecosystem"
        workspaces = {
          name = "netops"
        }
      }
    }


```


## Module exposed for AppDev/Ops teams to Consume

This module takes in networking requirements of the application and creates the relevant ACI policy objects.

Module contains Terraform resources for Application Profile, EPG, Contract, Filters, etc
Creation of the above ACI construct will result in creation of security groups, inbound and outbound rules and EC2 instance to EPG assignment.

### Module usage

```hcl
###
### App Networking setup Module provided by NetOps team to AppOps/Dev team
###

module app_network_definition {
  source = "./app_networking"                  # Module Source
  app = var.app                                # Application Name
  tiers = var.tiers                            # Application Tiers
  match_expression = var.match_expression      # Condition to assign EC2 instance to a Tier
  allow_policy = var.allow_policy              # Allowed destination Ports
  inet_access = var.inet_access                # Allow internet access to instances
}
```

## Remote Backend to save variables and state:
```
hostname     = "app.terraform.io"
    organization = "cisco-dcn-ecosystem"

    workspaces 
      name = "netops"

```

